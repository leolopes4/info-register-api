package br.com.pucminas.boasaude.integrations.authorizationserver.impl;

import br.com.pucminas.boasaude.integrations.authorizationserver.client.AuthorizationServerClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class AuthorizationIntegrationImpl {

    private AuthorizationServerClient authServerClient;

    @Autowired
    AuthorizationIntegrationImpl(AuthorizationServerClient authServerClient){
        this.authServerClient = authServerClient;
    }

    public boolean verifyIfApplicationIsAuthorized(String token){
        ResponseEntity<?> response = authServerClient.isAuthorized(token);

        if(HttpStatus.OK.value() == response.getStatusCodeValue()){
            return true;
        }else {
            return false;
        }
    }


}

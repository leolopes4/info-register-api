package br.com.pucminas.boasaude.integrations.authorizationserver.client;


import br.com.pucminas.boasaude.web.config.FeignConfiguration;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;

@FeignClient(name = "AuthorizationServerIntegration", url = "${integration.auth.server.url}", configuration = FeignConfiguration.class)
public interface AuthorizationServerClient {

    @GetMapping("/auth/validate")
    public ResponseEntity<?> isAuthorized(@RequestHeader("authorization") String token);
}

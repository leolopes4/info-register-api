package br.com.pucminas.boasaude.web.controller;

import br.com.pucminas.boasaude.model.dto.request.ProviderRequestDTO;
import br.com.pucminas.boasaude.model.dto.response.ProviderResponseDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.http.ResponseEntity;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

@Api(tags = "ProviderDocumentation")
public interface ApiProviderController {

    @ApiOperation(value = "Get all providers")
    public ResponseEntity<List<ProviderResponseDTO>> getAllProviders();

    @ApiOperation(value = "Get provider by id")
    public ResponseEntity<ProviderResponseDTO> getProvider(@ApiParam(value = "Provider id", required = true) Long id);

    @ApiOperation(value = "Get provider by specialization")
    public ResponseEntity<List<ProviderResponseDTO>> getProviderBySpecializationAndLocation(@ApiParam(value = "Provider specialization", required = true) String specialization, @ApiParam(value = "Provider location", required = true) String location);

    @ApiOperation(value = "Add new provider")
    public ResponseEntity<ProviderResponseDTO> addNewProvider(ProviderRequestDTO providerRequestDTO, UriComponentsBuilder uriBuilder);

    @ApiOperation(value = "Delete provider")
    public ResponseEntity<?> deleteProvider(@ApiParam(value = "Provider id", required = true) Long id);

    @ApiOperation(value = "Update data provider")
    public ResponseEntity<?> updateProvider(@ApiParam(value = "Provider id", required = true) Long id, ProviderRequestDTO providerRequestDTO);


}

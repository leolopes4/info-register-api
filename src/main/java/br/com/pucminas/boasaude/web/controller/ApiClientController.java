package br.com.pucminas.boasaude.web.controller;

import br.com.pucminas.boasaude.model.dto.request.ClientRequestDTO;
import br.com.pucminas.boasaude.model.dto.response.ClientResponseDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.http.ResponseEntity;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

@Api(tags = "ClientDocumentation")
public interface ApiClientController {

    @ApiOperation(value = "Get all clients")
    public ResponseEntity<List<ClientResponseDTO>> getAllClients();

    @ApiOperation(value = "Get client by id")
    public ResponseEntity<ClientResponseDTO> getClient(@ApiParam(value = "Client id", required = true) Long id);

    @ApiOperation(value = "Add new client")
    public ResponseEntity<ClientResponseDTO> addNewClient(ClientRequestDTO clientRequestDTO, UriComponentsBuilder uriBuilder);

    @ApiOperation(value = "Delete client")
    public ResponseEntity<?> deleteClient(@ApiParam(value = "Client id", required = true) Long id);

    @ApiOperation(value = "Update data client")
    public ResponseEntity<?> updateClient(@ApiParam(value = "Client id", required = true) Long id, ClientRequestDTO newUserDTO);

}

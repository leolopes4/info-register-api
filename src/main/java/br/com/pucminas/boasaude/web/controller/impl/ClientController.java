package br.com.pucminas.boasaude.web.controller.impl;

import java.util.List;

import javax.transaction.Transactional;
import javax.validation.Valid;

import br.com.pucminas.boasaude.model.dto.request.ClientRequestDTO;
import br.com.pucminas.boasaude.model.dto.response.ClientResponseDTO;
import br.com.pucminas.boasaude.services.ClientService;
import br.com.pucminas.boasaude.web.controller.ApiClientController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;


@RestController
@RequestMapping("/api/v1")
public class ClientController implements ApiClientController {

	private final ClientService clientService;

	@Autowired
	public ClientController(ClientService userService){
		this.clientService = userService;
	}

	@GetMapping("/clients")
	public ResponseEntity<List<ClientResponseDTO>> getAllClients() {

		return clientService.findAll();
	}
	
	@GetMapping("/client/{id}")
	public ResponseEntity<ClientResponseDTO> getClient(@PathVariable Long id) {
		return clientService.findById(id);
	}

	@GetMapping("/client")
	public ResponseEntity<ClientResponseDTO> getClientByDocument(@RequestParam String document) {
		return clientService.findByDocument(document);
	}
	
	@PostMapping("/client")
	public ResponseEntity<ClientResponseDTO> addNewClient(@RequestBody @Valid ClientRequestDTO clientRequestDTO, UriComponentsBuilder uriBuilder) {
		return clientService.save(clientRequestDTO, uriBuilder);
	}
	
	@DeleteMapping("/client/{id}")
	public ResponseEntity<?> deleteClient(@PathVariable Long id) {
		return clientService.deleteById(id);
	}
	
	@PutMapping("/client/{id}")
	@Transactional
	public ResponseEntity<?> updateClient(@PathVariable Long id, @RequestBody @Valid ClientRequestDTO newUserDTO){
		return clientService.updateById(id, newUserDTO);
	}

	@PatchMapping("/client/{id}")
	@Transactional
	public ResponseEntity<?> updatePasswordClient(@PathVariable Long id, @RequestBody @Valid ClientRequestDTO newUserDTO){
		return clientService.updateById(id, newUserDTO);
	}
}

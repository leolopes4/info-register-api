package br.com.pucminas.boasaude.web.controller.impl;

import br.com.pucminas.boasaude.model.dto.request.ProviderRequestDTO;
import br.com.pucminas.boasaude.model.dto.response.ProviderResponseDTO;
import br.com.pucminas.boasaude.services.ProviderService;
import br.com.pucminas.boasaude.web.controller.ApiProviderController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.transaction.Transactional;
import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class ProviderController implements ApiProviderController {

    private ProviderService providerService;

    @Autowired
    ProviderController(ProviderService providerService){
        this.providerService = providerService;
    }

    @GetMapping("/providers")
    public ResponseEntity<List<ProviderResponseDTO>> getAllProviders() {

        return this.providerService.findAll();
    }

    @GetMapping("/provider/{id}")
    public ResponseEntity<ProviderResponseDTO> getProvider(@PathVariable Long id) {

        return this.providerService.findById(id);
    }

    @GetMapping("/providers/{specialization}/{location}")
    public ResponseEntity<List<ProviderResponseDTO>> getProviderBySpecializationAndLocation(@PathVariable String specialization, @PathVariable String location) {
        return this.providerService.findBySpecializationAndLocation(specialization, location);
    }

    @PostMapping("/provider")
    public ResponseEntity<ProviderResponseDTO> addNewProvider(@RequestBody ProviderRequestDTO providerRequestDTO, UriComponentsBuilder uriBuilder) {
        return this.providerService.save(providerRequestDTO, uriBuilder);
    }

    @DeleteMapping("/provider/{id}")
    public ResponseEntity<?> deleteProvider(@PathVariable Long id) {

        return this.providerService.deleteById(id);
    }

    @PutMapping("/provider/{id}")
    @Transactional
    public ResponseEntity<?> updateProvider(@PathVariable Long id, @RequestBody ProviderRequestDTO providerRequestDTO) {

        return this.providerService.updateById(id, providerRequestDTO);
    }
}

package br.com.pucminas.boasaude.model.dto.request;

import br.com.pucminas.boasaude.model.entities.ClientEntity;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ClientRequestDTO {

    private String firstName;
    private String lastName;
    private String document;
    private String email;
    private String password;
    private String planType;


    public ClientEntity toEntity(){
        return new ClientEntity(this.firstName, this.lastName, this.document, this.email, this.password, this.planType);
    }
}

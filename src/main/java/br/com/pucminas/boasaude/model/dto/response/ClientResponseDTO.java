package br.com.pucminas.boasaude.model.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ClientResponseDTO {

    private String firstName;
    private String lastName;
    private String document;
    private String email;
    private String planType;

}
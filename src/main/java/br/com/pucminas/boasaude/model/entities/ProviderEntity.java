package br.com.pucminas.boasaude.model.entities;

import br.com.pucminas.boasaude.model.dto.request.ProviderRequestDTO;
import br.com.pucminas.boasaude.model.dto.response.ProviderResponseDTO;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
@Data
@RequiredArgsConstructor
@NoArgsConstructor
@Table(name = "Provider")
public class ProviderEntity {
	
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@NotNull @NotEmpty @Length(max = 20) @NonNull
	private String firstName;
	
	@NotNull @NotEmpty @Length(max = 20) @NonNull
	private String lastName;
	
	@NotNull @NotEmpty @Length(max = 20) @NonNull
	private String document;
	
	@NotNull @NotEmpty @Email @NonNull
	private String email;

	@NotNull @NotEmpty @NonNull
	private String specialization;

	@NotNull @NotEmpty @NonNull
	private String location;

	
	public void update(ProviderEntity newUserEntity) {
		this.firstName = newUserEntity.getFirstName();
		this.lastName = newUserEntity.getLastName();
		this.document = newUserEntity.getDocument();
		this.email = newUserEntity.getEmail();
		this.specialization = newUserEntity.getSpecialization();
		this.location = newUserEntity.getLocation();
	}

	public ProviderRequestDTO toDTO(){
		return new ProviderRequestDTO(this.firstName, this.lastName, this.document, this.email, this.specialization, this.location);
	}

	public ProviderResponseDTO toResponseDTO(){
		return new ProviderResponseDTO(this.firstName, this.lastName, this.document, this.email, this.specialization, this.location);
	}
	
	
	
}

package br.com.pucminas.boasaude.model.entities;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import br.com.pucminas.boasaude.model.dto.request.ClientRequestDTO;
import br.com.pucminas.boasaude.model.dto.response.ClientResponseDTO;
import lombok.*;
import org.hibernate.validator.constraints.Length;

@Entity
@Data
@RequiredArgsConstructor
@NoArgsConstructor
@Table(name = "Client")
public class ClientEntity {
	
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@NotNull @NotEmpty @Length(max = 20) @NonNull
	private String firstName;
	
	@NotNull @NotEmpty @Length(max = 20) @NonNull
	private String lastName;
	
	@NotNull @NotEmpty @Length(max = 20) @NonNull
	private String document;
	
	@NotNull @NotEmpty @Email @NonNull
	private String email;

	@NotNull @NotEmpty @Length(max = 50) @NonNull
	private String password;

	@NotNull @NotEmpty @Length(max = 50) @NonNull
	private String planType;

	
	public void update(ClientEntity newUserEntity) {
		this.firstName = newUserEntity.getFirstName();
		this.lastName = newUserEntity.getLastName();
		this.document = newUserEntity.getDocument();
		this.email = newUserEntity.getEmail();
		this.password = newUserEntity.getPassword();
		this.planType = newUserEntity.getPlanType();
	}

	public ClientRequestDTO toDTO(){
		return new ClientRequestDTO(this.firstName, this.lastName, this.document, this.email, this.password, this.planType);
	}

	public ClientResponseDTO toResponseDTO(){
		return new ClientResponseDTO(this.firstName, this.lastName, this.document, this.email, this.planType);
	}
	
	
	
}

package br.com.pucminas.boasaude.model.dto.request;

import br.com.pucminas.boasaude.model.entities.ProviderEntity;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ProviderRequestDTO {

    private String firstName;
    private String lastName;
    private String document;
    private String email;
    private String specialization;
    private String location;

    public ProviderEntity toEntity(){
        return new ProviderEntity(this.firstName, this.lastName, this.document, this.email, this.specialization, this.location);
    }
}

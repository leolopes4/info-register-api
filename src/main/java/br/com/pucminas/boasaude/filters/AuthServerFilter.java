package br.com.pucminas.boasaude.filters;

import br.com.pucminas.boasaude.integrations.authorizationserver.client.AuthorizationServerClient;
import br.com.pucminas.boasaude.integrations.authorizationserver.impl.AuthorizationIntegrationImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Component
@NoArgsConstructor
public class AuthServerFilter implements Filter {

    private AuthorizationIntegrationImpl authServerClient;
    private final ObjectMapper mapper = new ObjectMapper();

    @Autowired
    AuthServerFilter(AuthorizationIntegrationImpl authServerClient){
        this.authServerClient = authServerClient;
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        String token = request.getHeader("authorization");

        System.out.println(token);

        if(null != token){
            boolean isAuthorized = this.authServerClient.verifyIfApplicationIsAuthorized(token);

            if(isAuthorized){
                filterChain.doFilter(request, response);
            }else {
                Map<String, Object> errorDetails = new HashMap<>();
                errorDetails.put("messageError", "Invalid token");

                response.setStatus(HttpStatus.UNAUTHORIZED.value());
                response.setContentType(MediaType.APPLICATION_JSON_VALUE);

                mapper.writeValue(response.getWriter(), errorDetails);
            }


        }else {
            Map<String, Object> errorDetails = new HashMap<>();
            errorDetails.put("messageError", "Missing token");

            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            response.setContentType(MediaType.APPLICATION_JSON_VALUE);

            mapper.writeValue(response.getWriter(), errorDetails);
        }
    }

    @Override
    public void destroy() {

    }
}

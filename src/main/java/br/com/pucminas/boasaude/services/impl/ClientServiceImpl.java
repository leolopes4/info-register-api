package br.com.pucminas.boasaude.services.impl;

import br.com.pucminas.boasaude.model.dto.request.ClientRequestDTO;
import br.com.pucminas.boasaude.model.dto.response.ClientResponseDTO;
import br.com.pucminas.boasaude.model.entities.ClientEntity;
import br.com.pucminas.boasaude.repository.ClientRepository;
import br.com.pucminas.boasaude.services.ClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

@Service
public class ClientServiceImpl implements ClientService {

    private final ClientRepository clientRepo;
    private Logger logger = LoggerFactory.getLogger(ClientServiceImpl.class);

    @Autowired
    public ClientServiceImpl(ClientRepository userRepo){
        this.clientRepo = userRepo;
    }

    @Override
    public ResponseEntity<List<ClientResponseDTO>> findAll() {
        var clientsUntity = clientRepo.findAll();
        List<ClientResponseDTO> clientsResponseDTO = new ArrayList<ClientResponseDTO>();

        if(clientsUntity.isEmpty()){
            logger.info("[INFO-REGISTER ] - ::GET ALL CLIENTS:: - No Content - response: " );
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        }else {
            clientsUntity.forEach(client -> {
                clientsResponseDTO.add(client.toResponseDTO());
            });
        }
        var response = ResponseEntity.ok().body(clientsResponseDTO);

        logger.info("[INFO-REGISTER ] - ::GET ALL CLIENTS:: - Successful - response: " + response);
        return ResponseEntity.ok().body(clientsResponseDTO);
    }

    @Override
    public ResponseEntity<ClientResponseDTO> findById(Long id) {
        var clientEntity = clientRepo.findById(id);

        if (clientEntity.isPresent()) {
            var response = clientEntity.get().toResponseDTO();

            logger.info("[INFO-REGISTER ] - ::GET CLIENT BY ID:: - Successful - id: " + id + "response: " + response);
            return ResponseEntity.ok().body(response);
        }else{
            logger.info("[INFO-REGISTER ] - ::GET CLIENT BY ID:: - Not found - response: ");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    @Override
    public ResponseEntity<ClientResponseDTO> save(ClientRequestDTO clientRequestDTO, UriComponentsBuilder uriBuilder) {

        ClientEntity clientEntity = clientRequestDTO.toEntity();
        try {
            var clientEntitySaved = clientRepo.save(clientEntity);
            URI uri = uriBuilder.path("api/v1/client/{id}").buildAndExpand(clientEntity.getId()).toUri();
            var response = clientEntitySaved.toResponseDTO();

            logger.info("[INFO-REGISTER ] - ::ADD NEW CLIENT:: - Successful - response: " + response + " uri: " + uri);
            return ResponseEntity.created(uri).body(response);
        }catch (Exception e){
            logger.error("[INFO-REGISTER ] - ::ADD NEW CLIENT:: - Fail - exception: " + e.getLocalizedMessage());
        }

        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }

    @Override
    public ResponseEntity<?> deleteById(Long id) {
        var clientEntity = clientRepo.findById(id);

        if(clientEntity.isPresent()){
            clientRepo.deleteById(id);
            logger.info("[INFO-REGISTER ] - ::DELETE CLIENT:: - Successful - id: " + id);
            return ResponseEntity.ok().build();
        }

        logger.info("[INFO-REGISTER ] - ::DELETE CLIENT:: - No Found - id: " + id);

        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();

    }

    @Override
    public ResponseEntity<?> updateById(Long id, ClientRequestDTO updatedClientRequestDTO) {
        var clientEntity = clientRepo.findById(id);

        if(clientEntity.isPresent()){
            clientEntity.get().update(updatedClientRequestDTO.toEntity());

            logger.info("[INFO-REGISTER ] - ::UPDATE CLIENT:: - Successful - id: " + id);
            return ResponseEntity.ok().build();
        }

        logger.info("[INFO-REGISTER ] - ::UPDATE CLIENT:: - No Found - id: " + id);

        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();

    }

    @Override
    public ResponseEntity<ClientResponseDTO> findByDocument(String document){
        var clientEntity = clientRepo.findByDocument(document);

        if (clientEntity.isPresent()) {
            var response = clientEntity.get().toResponseDTO();

            logger.info("[INFO-REGISTER ] - ::GET CLIENT BY DOCUMENT:: - Successful - DOCUMENT: " + document + ", response: " + response);
            return ResponseEntity.ok().body(response);
        }else{
            logger.info("[INFO-REGISTER ] - ::GET CLIENT BY DOCUMENT:: - Not found - response: ");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }
}

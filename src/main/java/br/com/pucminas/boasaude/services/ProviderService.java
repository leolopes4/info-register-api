package br.com.pucminas.boasaude.services;

import br.com.pucminas.boasaude.model.dto.request.ProviderRequestDTO;
import br.com.pucminas.boasaude.model.dto.response.ProviderResponseDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

public interface ProviderService {

    public ResponseEntity<List<ProviderResponseDTO>> findAll();
    public ResponseEntity<ProviderResponseDTO> findById(Long id);
    public ResponseEntity<List<ProviderResponseDTO>> findBySpecializationAndLocation(String specialization, String Location);
    public ResponseEntity<ProviderResponseDTO> save(ProviderRequestDTO providerRequestDTO, UriComponentsBuilder uriBuilder);
    public ResponseEntity<?> deleteById(Long id);
    public ResponseEntity<?> updateById(Long id, ProviderRequestDTO updatedProviderDTO);


}

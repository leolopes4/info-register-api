package br.com.pucminas.boasaude.services.impl;

import br.com.pucminas.boasaude.model.dto.request.ProviderRequestDTO;
import br.com.pucminas.boasaude.model.dto.response.ProviderResponseDTO;
import br.com.pucminas.boasaude.model.entities.ProviderEntity;
import br.com.pucminas.boasaude.repository.ProviderRepository;
import br.com.pucminas.boasaude.services.ProviderService;
import org.aspectj.apache.bcel.classfile.Module;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

@Service
public class ProviderServiceImpl implements ProviderService {

    private final ProviderRepository providerRepo;
    private Logger logger = LoggerFactory.getLogger(ProviderServiceImpl.class);

    @Autowired
    public ProviderServiceImpl(ProviderRepository providerRepo){
        this.providerRepo = providerRepo;
    }

    @Override
    public ResponseEntity<List<ProviderResponseDTO>> findAll() {
        var providersUntity = providerRepo.findAll();
        List<ProviderResponseDTO> providersResponseDTO = new ArrayList<ProviderResponseDTO>();

        if(providersUntity.isEmpty()){
            logger.info("[INFO-REGISTER ] - ::GET ALL PROVIDERS:: - No Content - response: " );
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        }else {
            providersUntity.forEach(provider -> {
                providersResponseDTO.add(provider.toResponseDTO());
            });
        }
        var response = ResponseEntity.ok().body(providersResponseDTO);

        logger.info("[INFO-REGISTER ] - ::GET ALL PROVIDERS:: - Successful - response: " + response);
        return ResponseEntity.ok().body(providersResponseDTO);
    }

    @Override
    public ResponseEntity<ProviderResponseDTO> findById(Long id) {
        var providerEntity = providerRepo.findById(id);

        if (providerEntity.isPresent()) {
            var response = providerEntity.get().toResponseDTO();

            logger.info("[INFO-REGISTER ] - ::GET PROVIDER BY ID:: - Successful - id: " + id + "response: " + response);
            return ResponseEntity.ok().body(response);
        }else{
            logger.info("[INFO-REGISTER ] - ::GET PROVIDER BY ID:: - Not found - response: ");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    @Override
    public ResponseEntity<List<ProviderResponseDTO>> findBySpecializationAndLocation(String specialization, String location){

        var providersEntity = providerRepo.findBySpecialization(specialization, location);
        List<ProviderResponseDTO> providersList = new ArrayList<ProviderResponseDTO>();

        if (providersEntity.isPresent()) {
            providersEntity.get().forEach(providerEntity -> {
                if(providerEntity.getSpecialization().equals(specialization)){
                    providersList.add(providerEntity.toResponseDTO());
                }
            });

            logger.info("[INFO-REGISTER ] - ::GET PROVIDER BY SPECIALIZATION:: - Successful");
            return ResponseEntity.ok().body(providersList);
        }else{
            logger.info("[INFO-REGISTER ] - ::GET PROVIDER BY SPECIALIZATION:: - Not found - response: ");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    @Override
    public ResponseEntity<ProviderResponseDTO> save(ProviderRequestDTO providerRequestDTO, UriComponentsBuilder uriBuilder) {

        ProviderEntity providerEntity = providerRequestDTO.toEntity();
        try {
            var providerEntitySaved = providerRepo.save(providerEntity);
            URI uri = uriBuilder.path("api/v1/provider/{id}").buildAndExpand(providerEntity.getId()).toUri();
            var response = providerEntitySaved.toResponseDTO();

            logger.info("[INFO-REGISTER ] - ::ADD NEW PROVIDER:: - Successful - response: " + response + " uri: " + uri);
            return ResponseEntity.created(uri).body(response);
        }catch (Exception e){
            logger.error("[INFO-REGISTER ] - ::ADD NEW PROVIDER:: - Fail - exception: " + e.getLocalizedMessage());
        }

        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }

    @Override
    public ResponseEntity<?> deleteById(Long id) {
        var providerEntity = providerRepo.findById(id);

        if(providerEntity.isPresent()){
            providerRepo.deleteById(id);
            logger.info("[INFO-REGISTER ] - ::DELETE PROVIDER:: - Successful - id: " + id);
            return ResponseEntity.ok().build();
        }

        logger.info("[INFO-REGISTER ] - ::DELETE PROVIDER:: - No Found - id: " + id);

        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();

    }

    @Override
    public ResponseEntity<?> updateById(Long id, ProviderRequestDTO updatedProviderRequestDTO) {
        var providerEntity = providerRepo.findById(id);

        if(providerEntity.isPresent()){
            providerEntity.get().update(updatedProviderRequestDTO.toEntity());

            logger.info("[INFO-REGISTER ] - ::UPDATE PROVIDER:: - Successful - id: " + id);
            return ResponseEntity.ok().build();
        }

        logger.info("[INFO-REGISTER ] - ::UPDATE PROVIDER:: - No Found - id: " + id);

        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();

    }
}

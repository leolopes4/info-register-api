package br.com.pucminas.boasaude.services;

import br.com.pucminas.boasaude.model.dto.request.ClientRequestDTO;
import br.com.pucminas.boasaude.model.dto.response.ClientResponseDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

public interface ClientService {

    public ResponseEntity<List<ClientResponseDTO>> findAll();
    public ResponseEntity<ClientResponseDTO> findById(Long id);
    public ResponseEntity<ClientResponseDTO> findByDocument(String id);
    public ResponseEntity<ClientResponseDTO> save(ClientRequestDTO clientRequestDTO, UriComponentsBuilder uriBuilder);
    public ResponseEntity<?> deleteById(Long id);
    public ResponseEntity<?> updateById(Long id, ClientRequestDTO newUserDTO);


}

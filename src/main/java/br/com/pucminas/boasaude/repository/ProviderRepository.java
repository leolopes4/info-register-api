package br.com.pucminas.boasaude.repository;

import br.com.pucminas.boasaude.model.entities.ClientEntity;
import br.com.pucminas.boasaude.model.entities.ProviderEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProviderRepository extends JpaRepository<ProviderEntity, Long>{
	
	ClientEntity findByFirstName(String name);

	List<ProviderEntity> findAll();

	@Override
	Optional<ProviderEntity> findById(Long id);


	@Query(value = "SELECT * FROM PROVIDER WHERE SPECIALIZATION = ?1 AND LOCATION = ?2", nativeQuery = true)
	Optional<List<ProviderEntity>> findBySpecialization(String specialization, String location);
}

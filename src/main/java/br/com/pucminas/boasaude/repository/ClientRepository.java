package br.com.pucminas.boasaude.repository;

import br.com.pucminas.boasaude.model.entities.ClientEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ClientRepository extends JpaRepository<ClientEntity, Long>{

	List<ClientEntity> findAll();

	@Override
	Optional<ClientEntity> findById(Long id);

	Optional<ClientEntity> findByDocument(String document);


}
